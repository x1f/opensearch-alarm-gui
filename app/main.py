from typing import Union, List, Dict
from fastapi import FastAPI, Depends, Request, HTTPException, Body, status
from pydantic import BaseModel
from fastapi.middleware.cors import CORSMiddleware
from fastapi.templating import Jinja2Templates
from fastapi.staticfiles import StaticFiles
from fastapi.responses import HTMLResponse
from fastapi.security import HTTPBasic, HTTPBasicCredentials

from databases import Database
from json import JSONDecodeError
from pathlib import Path

import os
import secrets
import datetime, time
import dateutil.parser as dp

BASE_PATH = Path(__file__).resolve().parent
TEMPLATES = Jinja2Templates(directory=str(BASE_PATH / "templates"))
INTERVAL = 18000 # cleanup interval 1h

async def update_alerts(monitor, alert):
    query = ''
    ts = dp.parse(monitor['period_end']).strftime('%s')

    if (alert['type'] == 'new'):
        query = """INSERT INTO alarms (id, monitor_name, trigger_name, severity, period_start, period_end, timestamp, text) VALUES ('{}', '{}', '{}', {}, '{}', '{}', {}, '{}')""".format(
            alert['id'], monitor['name'], monitor['trigger'], monitor['severity'], monitor['period_start'], monitor['period_end'], ts, alert['text']
        )

    if (alert['type'] == 'deduped'):
        #print ("update alert ")
        #print (alert)
        query = """SELECT * FROM alarms WHERE id = '{}'""".format(alert['id'])
        result = await db.fetch_all(query=query)
        if (not result):
            print("INFO: record doesnt exist, creating it")
            query = """INSERT INTO alarms (id, monitor_name, trigger_name, severity, period_start, period_end, timestamp, text) VALUES ('{}', '{}', '{}', {}, '{}', '{}', {}, '{}')""".format(
                alert['id'], monitor['name'], monitor['trigger'], monitor['severity'], monitor['period_start'], monitor['period_end'], ts, alert['text'])
        try:
            #print(query)
            result = await db.execute(query=query)
        except Exception as e:
            print ("error %s" % e)

        query = """UPDATE alarms SET monitor_name='{}', trigger_name='{}', severity={}, period_start='{}', period_end='{}', timestamp={}, text='{}' WHERE id='{}'""".format(
            monitor['name'], monitor['trigger'], monitor['severity'], monitor['period_start'], monitor['period_end'], ts, alert['text'], alert['id'])

    if (alert['type'] == 'completed'):
        query = "DELETE FROM alarms WHERE id = '{}'".format(alert['id'])

    if (query):
        try:
            print("INFO: {}".format(query))
            return await db.execute(query=query)
        except Exception as e:
            print ("error %s" % e)



app = FastAPI()
app.mount('/static', StaticFiles(directory=str(BASE_PATH / "static")),name="static")
db = Database("sqlite+aiosqlite:///data/alarm.db")
security = HTTPBasic()
name = os.getenv('NAME') or ''
version = os.getenv('VERSION') or ''

def authorize(credentials: HTTPBasicCredentials = Depends(security)):
    is_user_ok = secrets.compare_digest(credentials.username, os.getenv('LOGIN') or '')
    is_pass_ok = secrets.compare_digest(credentials.password, os.getenv('PASSWORD') or '')

    if not (is_user_ok and is_pass_ok):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail='Incorrect user or password.',
            headers={'WWW-Authenticate': 'Basic'},
        )


def filter_datetime(input):
    datetime = dp.parse(input).strftime('%Y-%m-%d %H:%M:%S %Z')
    return datetime

TEMPLATES.env.filters["datetime"] = filter_datetime


class Notification(BaseModel):
    monitor: dict
    alert: dict[str, str, str]

async def cleanup_database():
    t = int(time.time())
    query = """SELECT * FROM alarms WHERE timestamp < {}""".format(t-INTERVAL)
    #print(query)
    results = await db.fetch_all(query=query)
    if (results):
        for result in results:
            print("""INFO: cleanup old entry {} - {} - {}""".format(result.monitor_name, result.trigger_name, result.text))
            query = "DELETE FROM alarms WHERE timestamp < {}".format(t)
            await db.execute(query=query)


@app.on_event("startup")
async def db_connect():
    await db.connect()
    print("INFO: DB connected, creating table")
    query = """CREATE TABLE IF NOT EXISTS alarms (id VARCHAR(255) PRIMARY KEY, monitor_name VARCHAR(255), trigger_name VARCHAR(255), severity INTEGER, period_start VARCHAR(255), period_end VARCHAR(255), timestamp INTEGER, text VARCHAR(255))"""
    await db.execute(query=query)


@app.on_event("shutdown")
async def db_disconnect():
    await cleanup_database()
    await db.disconnect()
    print("INFO: DB disconnected")


@app.get("/", dependencies=[Depends(authorize)])
async def root(request: Request):
    await cleanup_database()
    query = "SELECT * FROM alarms"
    results = await db.fetch_all(query=query)
    return TEMPLATES.TemplateResponse(
        "index.html.j2",
        {"request": request, "results": results, "time": datetime.datetime.now().isoformat(), "version": version, "name": name},
    )


@app.get("/check_mk", dependencies=[Depends(authorize)])
async def get_alarms_check_mk(request: Request, monitor: str = '', trigger: str = ''):
    if (not monitor and not trigger):
        html_content = """2 Opensearch_Alarms - Monitor_name is missing!\n"""
        return HTMLResponse(content=html_content, status_code=404)

    await cleanup_database()
    query = "SELECT * FROM alarms"

    if (monitor):
        query = """ {} WHERE monitor_name='{}'""".format(query, monitor)
    if (trigger):
        query = """ {} WHERE trigger_name='{}'""".format(query, trigger)

    results = await db.fetch_all(query=query)
    count = len(results)

    if (count <= 0):
        errlevel = 0
    elif (count <=3):
        errlevel = 1
    else:
        errlevel = 2

    text = []
    for i in results:
        text.append(i.text)

    if (monitor):
        name = monitor
    if (trigger):
        name = trigger

    html_content = """{} Opensearch_{} count={} {}\n""".format(errlevel, name.replace(' ','_'), count, ' __ '.join(text))
    return HTMLResponse(content=html_content, status_code=200)

@app.get("/alarms", dependencies=[Depends(authorize)])
async def get_all():
    query = "SELECT * FROM alarms"
    results = await db.fetch_all(query=query)

    return  results

@app.post('/notifications')
async def notifications(payload: Notification):
    result= await update_alerts(payload.monitor, payload.alert)
    return result
