#FROM python:3.10-bullseye
FROM python:3.10-slim

ARG VERSION
ARG NAME

ENV DESCRIPTION AlarmGUI provides a easy Overview about current Opensearch Alarms
ENV VENDOR https://github.com/1randy/opensearch-alarm-gui
ENV NAME=$NAME
ENV VERSION=$VERSION
ENV DEBIAN_FRONTEND noninteractive

MAINTAINER matrix technology GmbH <info@matrix.ag>
LABEL Description=${DESCRIPTION} Vendor=${VENDOR} Version=${VERSION}


WORKDIR /code
COPY ./requirements.txt /code/requirements.txt
RUN pip install --no-cache-dir --upgrade -r /code/requirements.txt
COPY ./app /code/app

CMD ["uvicorn", "app.main:app", "--proxy-headers", "--host", "0.0.0.0", "--port", "80", "--reload"]

